import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

// gRPC dependencies
val grpcVersion = "1.33.0"
val grpcKotlinVersion = "0.2.1"
val grpcKotlinExt = "jdk7@jar"
val protobufVersion = "3.14.0"

// gRPC directory configurations
val grpcGeneratedDir = ".grpc_generated"
val grpcOutputSubDir = "grpc"
val grpcKtOutputSubDir = "grpckt"

// fuel dependencies
val fuelVersion = "2.3.0"
val fuelKotlinxSerialization = "0.10.0"

// Kotlinx dependencies
val serializationVersion = "1.0.1"
val coroutinesVersion = "1.4.1"


buildscript {
    extra["kotlinVersion"] = "1.4.10"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.10")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlinVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-allopen:${project.extra["kotlinVersion"]}")
    }
}

plugins {
    `maven-publish`
    `java-library`
    java
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"
    id("com.google.protobuf") version "0.8.14"
}

group = "ai.maum.lib"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    google()
}

dependencies {
    implementation(kotlin("stdlib"))

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")

    implementation("org.slf4j:slf4j-api:1.7.30")

    implementation("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")
    implementation("io.grpc:grpc-netty:$grpcVersion")
    //implementation("io.grpc:grpc-protobuf:$grpcVersion")
    //implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-stub:$grpcVersion")
    runtimeOnly("io.grpc:grpc-netty-shaded:$grpcVersion")

    implementation("com.github.kittinunf.fuel:fuel:$fuelVersion")
//    implementation("com.github.kittinunf.fuel:fuel-kotlinx-serialization:$fuelKotlinxSerialization")

    testImplementation("junit:junit:4.12") 
}

kotlin {
    sourceSets["main"].apply {
        kotlin.srcDir("$projectDir/$grpcGeneratedDir/main/$grpcKtOutputSubDir")
    }
}

sourceSets {
    main {
        proto {
            // In addition to the default 'src/main/proto'
            srcDir("src/main/protobuf")
            srcDir("src/main/protocolbuffers")
            // In addition to the default '**/*.proto' (use with caution).
            // Using an extension other than 'proto' is NOT recommended,
            // because when proto files are published along with class files, we can
            // only tell the type of a file from its extension.
            include("**/*.protodevel")
        }
        java {
            srcDir("$grpcGeneratedDir/main/$grpcOutputSubDir")
            srcDir("$grpcGeneratedDir/main/java")
        }
    }
    test {
        proto {
            // In addition to the default 'src/test/proto'
            srcDir("src/test/protocolbuffers")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }

    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:$grpcKotlinExt"
        }
    }
    /*
     * (For plugins; ex:grpc, grpckt)
     * Generated stub files will be located at:
     * $generatedFilesBaseDir/$sourceSet/$outputSubDir/{java_package declared in .proto}
     *
     * (For builtins; ex:java)
     * Generated core files will be located at:
     * $generatedFilesBaseDir/$sourceSet/$builtinPluginName
     */
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            task.builtins {
                // remove("java")
            }
            task.plugins {
                id("grpc") {
                    outputSubDir = grpcOutputSubDir
                }
                id("grpckt") {
                    outputSubDir = grpcKtOutputSubDir
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/$grpcGeneratedDir"
}

publishing {
    publications {
        create<MavenPublication>("default") {
            groupId = groupId
            artifactId = "lib-stats"
            version = version

            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://aics-nexus.maum.ai/repository/api-2-snapshots")
            credentials {
                username = "admin"
                password = "Mindslab!1"
            }
        }
    }
}

tasks.clean {
    delete("$projectDir/$grpcGeneratedDir")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
