package ai.maum.lib.utils

const val ILLEGAL_ARGUMENT_EMAIL = "email is null"
const val ILLEGAL_ARGUMENT_CLIENT_ID = "client id is null"
const val ILLEGAL_ARGUMENT_CLIENT_SECRET = "client secret is null"

const val HEADER_CONTENT_TYPE_JSON = "application/json"