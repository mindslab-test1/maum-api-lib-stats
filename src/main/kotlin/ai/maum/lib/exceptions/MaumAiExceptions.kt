package ai.maum.lib.exceptions

open class MaumAiBaseException(
        override val message: String
) : RuntimeException(message)

class MaumAiResponseException(
        val statusCode: Int,
        override var message: String,
): MaumAiBaseException(message = message){
    constructor(statusCode: Int): this(statusCode, getResponseMessage(statusCode)){
        message = when(statusCode){
            400 -> "bad request"
            401 -> "unathorized"
            403 -> "forbidden"
            404 -> "not found"
            500 -> "internal server error"
            else -> "unknown status code: $statusCode"
        }
    }
}

fun getResponseMessage(statusCode: Int): String {
    return when(statusCode){
        400 -> "bad request"
        401 -> "unathorized"
        403 -> "forbidden"
        404 -> "not found"
        500 -> "internal server error"
        else -> "unknown status code: $statusCode"
    }
}